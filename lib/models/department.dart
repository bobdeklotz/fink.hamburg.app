class Department {
  final String name;
  final int id;

  const Department(this.name, this.id);

  @override
  String toString() {
    return 'Department{name: $name, id: $id}';
  }
}

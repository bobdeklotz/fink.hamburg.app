import 'dart:collection';

import 'package:finkapp/models/article.dart';
import 'package:html/parser.dart';

class ArticleList {
  List<Article> articles;

  ArticleList.fromJson(List<dynamic> json) {
    List<Map<String, dynamic>> jsonmaps = List();
    json.forEach((elem) => jsonmaps.add(elem));
    articles = List();
    jsonmaps.forEach((Map<String, dynamic> entry) {
      int id = entry['id'];
      String link = parse(entry['link'])?.body?.innerHtml;
      Map<String, dynamic> rawTitle = entry['title'];
      String title = rawTitle == null
          ? null
          : parse(rawTitle['rendered'])?.body?.innerHtml;
      Map<String, dynamic> rawExcerpt = entry['excerpt'];
      String excerpt = rawExcerpt == null
          ? null
          : parse(rawExcerpt['rendered'])?.body?.innerHtml;
      int mediaId = entry['featured_media'];
      bool protected =
          entry['content'] == null ? false : entry['content']['protected'];
      if (!protected) {
        articles.add(Article(id, link, title, excerpt, mediaId, ''));
      }
    });
  }
}

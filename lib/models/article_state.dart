import 'package:finkapp/models/article.dart';

// The State represents the data the View requires. The View consumes a Stream
// of States. The view rebuilds every time the Stream emits a new State!
//
// The State Stream will emit new States depending on the situation: The
// initial state, loading states, the list of results, and any errors that
// happen.

class ArticleState {
  final int pageNumber;

  ArticleState(this.pageNumber);
}

class ArticleLoadedState extends ArticleState {
  final List<Article> articles;

  ArticleLoadedState(pageNumber, this.articles)
      : super(pageNumber);
}

class ArticleErrorState extends ArticleState {
  final int errorCode;

  ArticleErrorState(pageNumber, this.errorCode) : super(pageNumber);
}

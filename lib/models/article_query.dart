import 'package:finkapp/models/department.dart';

class ArticleQuery {
  final int pageNumber;
  final Department department;
  final String searchQueryString;

  ArticleQuery(this.pageNumber, this.department, this.searchQueryString);
}

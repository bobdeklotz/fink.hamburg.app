import 'package:finkapp/models/article.dart';

class FavoriteState {
  Article article;
  bool isFavorite;

  FavoriteState(this.article, this.isFavorite);

  @override
  String toString() {
    return 'FavoriteState{article: $article, isFavorite: $isFavorite}';
  }
}

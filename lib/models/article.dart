class Article {
  int id;
  String link;
  String title;
  String excerpt;

  // this represents the media used to retrieve the link to the thumbnail.
  // constructed link looks like https://fink.hamburg/wp-json/wp/v2/media/41922
  int mediaId;

  // gets filled in by ArticleService after retrieval of the linkToMedia
  String linkToThumbnail;

  Article(this.id, this.link, this.title, this.excerpt, this.mediaId, this.linkToThumbnail);

  Article.fromJson(Map<String, dynamic> jsonMap) {
    id = jsonMap['id'];
    link = jsonMap['link'];
    title = jsonMap['title'];
    excerpt = jsonMap['excerpt'];
    mediaId = jsonMap['mediaId'];
    linkToThumbnail = jsonMap['linkToThumbnail'];
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'title': title,
        'link': link,
        'mediaId': mediaId,
        'linkToThumbnail': linkToThumbnail
      };

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Article && runtimeType == other.runtimeType && id == other.id;

  @override
  int get hashCode => id.hashCode;

  @override
  String toString() {
    return 'Article{id: $id, title: $title}';
  }
}

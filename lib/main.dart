import 'package:finkapp/models/department.dart';
import 'package:finkapp/services/service_injector.dart';
import 'package:finkapp/widgets/overview_page.dart';
import 'package:flutter/material.dart';

void main() => runApp(new FinkApp());

class FinkApp extends StatefulWidget {
  static const ARTICLES_PER_PAGE = 10;
  static const BASE_URL = 'https://fink.hamburg/wp-json/wp/v2';
  static const APP_TITLE = 'FINK.HAMBURG';
  static const THUMBNAIL_SIZE = 'medium_large';
  static const PRIMARY_COLOR = Color.fromARGB(255, 253, 4, 53);
  static const ACCENT_COLOR = Color.fromARGB(255, 61, 20, 50);
  static const List<Department> DEPARTMENTS = <Department>[
    Department('Alle Artikel', null),
    Department('Allgemein', 1),
    Department('Featured Posts', 7),
    Department('Mensch & Umwelt', 75),
    Department('Technik', 76),
    Department('Kultur', 78),
    Department('Politik', 79),
    Department('Wirtschaft & Soziales', 77),
    Department('Bundestagswahl', 1082),
    Department('Best of Protest', 1201),
    Department('Faces of Filmfest', 1437),
    Department('Faktencheck', 1690),
    Department('Adventskalender', 1990),
    Department('Faces of Filmfest 2018', 2944),
    Department('Corona-News', 4390),
    Department('Corona', 4548),
    Department('Lesezeichen', null),
    Department('Impressum', null)
  ];
  static ThemeData themeData = ThemeData(
      fontFamily: 'RobotoCondensed',
      primaryColor: FinkApp.PRIMARY_COLOR,
      accentColor: FinkApp.ACCENT_COLOR);

  @override
  FinkAppState createState() {
    return FinkAppState();
  }
}

class FinkAppState extends State<FinkApp> {

  @override
  void initState() {
    super.initState();
    // OneSignal.shared.init('80502e2e-83b2-4468-9dbc-d99efeec8da2');
    ServiceInjector.init();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: FinkApp.APP_TITLE,
      theme: FinkApp.themeData,
      home: OverviewPageWidget(),
    );
  }
}

import 'package:finkapp/main.dart';
import 'package:finkapp/models/department.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class DepartmentDrawer extends StatelessWidget {
  final List<Department> _departmentList = FinkApp.DEPARTMENTS;
  final PublishSubject departmentStream;

  DepartmentDrawer(this.departmentStream);

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [FinkApp.ACCENT_COLOR, FinkApp.PRIMARY_COLOR]),
      ),
      child: ListView.builder(
        itemBuilder: (BuildContext context, int index) {
          Department department = _departmentList[index];
          return ListTile(
            title: Text(department.name.toUpperCase(),
                style: TextStyle(
                    fontSize: 16.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.white)),
            onTap: () {
              departmentStream.sink.add(department);
              Navigator.pop(context);
            },
          );
        },
        itemCount: _departmentList.length,
      ),
    ));
  }
}

import 'dart:async';

import 'package:finkapp/main.dart';
import 'package:finkapp/models/article.dart';
import 'package:finkapp/services/bookmark_service.dart';
import 'package:finkapp/services/service_injector.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class NativeArticlePageWidget extends StatefulWidget {
  final Article _article;

  NativeArticlePageWidget(this._article);

  @override
  State<StatefulWidget> createState() => NativeArticlePageState(_article);
}

/// https://github.com/flutter/plugins/blob/master/packages/webview_flutter/example/lib/main.dart

class NativeArticlePageState extends State<NativeArticlePageWidget> {
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  final Article _article;
  BookmarkService _bookmarkService;
  bool _isBookmark = false;

  NativeArticlePageState(this._article);

  @override
  void initState() {
    super.initState();
    _bookmarkService = ServiceInjector.provideBookmarkService();
    _bookmarkService.isBookmark(_article).then((isBookmark) {
      setState(() {
        _isBookmark = isBookmark;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(_article.title),
          backgroundColor: FinkApp.ACCENT_COLOR,
          actions: <Widget>[
            IconButton(
                icon: _drawBookmarkIcon(), onPressed: () => _toggleBookmark())
          ]),
      body: WebView(
        initialUrl: _article.link,
        onWebViewCreated: (WebViewController webViewController) {
          _controller.complete(webViewController);
        },),
    );
  }

  Icon _drawBookmarkIcon() {
    if (_isBookmark) {
      return Icon(Icons.bookmark, color: Colors.white);
    } else {
      return Icon(Icons.bookmark_border, color: Colors.white);
    }
  }

  void _toggleBookmark() {
    setState(() {
      _isBookmark = !_isBookmark;
      if (_isBookmark) {
        _bookmarkService.addBookmark(_article);
      } else {
        _bookmarkService.removeBookmark(_article);
      }
    });
  }
}

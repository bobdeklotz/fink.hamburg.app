import 'package:cache_image/cache_image.dart';
import 'package:finkapp/main.dart';
import 'package:finkapp/models/article.dart';
import 'package:finkapp/widgets/native_article_page.dart';
import 'package:flutter/material.dart';

class ArticleLoadedTile extends StatelessWidget {
  final Article _article;

  ArticleLoadedTile(this._article);

  @override
  Widget build(BuildContext context) {
    return Card(
        color: FinkApp.ACCENT_COLOR,
        child: InkWell(
          splashColor: Colors.white,
          onTap: () => _pushArticlePage(context, _article),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: <Widget>[
                Container(
                    width: MediaQuery.of(context).size.width,
                    child: Ink.image(
                        height: MediaQuery.of(context).size.height / 3.5,
                        fit: BoxFit.cover,
                        image:
                            CacheImage(_article.linkToThumbnail),
                        child: InkWell())),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(_article.title.toUpperCase(),
                      style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.white)),
                )
              ],
            ),
          ),
        ));
  }
}

void _pushArticlePage(BuildContext context, Article article) {
  Navigator.push(
      context,
      MaterialPageRoute(
          builder: (builder) => NativeArticlePageWidget(article)));
}

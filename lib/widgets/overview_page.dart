import 'package:finkapp/main.dart';
import 'package:finkapp/models/article.dart';
import 'package:finkapp/models/article_query.dart';
import 'package:finkapp/models/article_state.dart';
import 'package:finkapp/models/department.dart';
import 'package:finkapp/services/article_service.dart';
import 'package:finkapp/services/service_injector.dart';
import 'package:finkapp/widgets/drawer.dart';
import 'package:finkapp/widgets/imprint_page.dart';
import 'package:finkapp/widgets/overview_tiles.dart';
import 'package:finkapp/widgets/search_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:rxdart/rxdart.dart';

class OverviewPageWidget extends StatefulWidget {
  @override
  createState() => OverviewPageState();
}

class OverviewPageState extends State<OverviewPageWidget> {
  ArticleService _articleService = ServiceInjector.provideArticleService();
  RefreshController _refreshController =
      RefreshController(initialRefresh: true);

  List<Article> _articles = List();
  int _currentPageNumber;
  String _currentSearchQueryString;
  Department _currentDepartment = Department('Alle Artikel', null);
  PublishSubject _departmentStream = PublishSubject();

  @override
  void initState() {
    super.initState();

    _departmentStream.listen((departmentFromDrawer) {
      setState(() {
        _currentDepartment = departmentFromDrawer;

        setState(() {
          _resetArticleList();
        });
        _refreshController.requestRefresh();
      });
    });
  }

  Future<void> _loadArticles() async {
    ArticleQuery query = ArticleQuery(
        _currentPageNumber, _currentDepartment, _currentSearchQueryString);

    ArticleState articleState = await _articleService.fetchArticles(query);
    _handleArticleState(articleState);
    _currentPageNumber++;
  }

  void _handleArticleState(ArticleState articleState) async {
    if (articleState.runtimeType == ArticleLoadedState) {
      ArticleLoadedState articleLoadedState =
          articleState as ArticleLoadedState;
      if (articleLoadedState.pageNumber == 1) {
        setState(() {
          _resetArticleList();
        });
        await Future.delayed(Duration(milliseconds: 30));
      }
      setState(() {
        _articles.addAll(articleLoadedState.articles);
      });
    } else {
      ArticleErrorState articleErrorState = articleState as ArticleErrorState;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(FinkApp.APP_TITLE),
          backgroundColor: FinkApp.ACCENT_COLOR,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.search),
              onPressed: () {
                showSearch(
                  context: context,
                  delegate: SearchPage(),
                );
              },
            ),
          ]),
      drawer: DepartmentDrawer(_departmentStream),
      body: _buildBodyDependingOnDepartment(),
    );
  }

  Widget _buildBodyDependingOnDepartment() {
    if (_currentDepartment.name == 'Impressum') {
      return ImprintPage();
    } else {
      return SmartRefresher(
        enablePullDown: true,
        enablePullUp: true,
        header: MaterialClassicHeader(),
        footer: _buildLoadingFooter(),
        controller: _refreshController,
        onRefresh: _onRefresh,
        onLoading: _onLoading,
        child: ListView.builder(
            itemCount: _articles.length,
            itemBuilder: (context, index) {
              return ArticleLoadedTile(_articles[index]);
            }),
      );
    }
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  void _onRefresh() async {
    _currentPageNumber = 1;
    await _loadArticles();
    // if failed,use refreshFailed()
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    await _loadArticles();
    // if failed,use loadFailed(),if no data return,use LoadNodata()
    _refreshController.loadComplete();
  }

  void _resetArticleList() {
    _articles = List();
  }

  CustomFooter _buildLoadingFooter() {
    return CustomFooter(
      builder: (BuildContext context, LoadStatus mode) {
        Widget body;
        if (mode == LoadStatus.idle) {
          body = CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(FinkApp.ACCENT_COLOR));
        } else if (mode == LoadStatus.loading) {
          body = CircularProgressIndicator(
              valueColor: AlwaysStoppedAnimation<Color>(FinkApp.ACCENT_COLOR));
        } else if (mode == LoadStatus.failed) {
          body = Text("Load Failed!Click retry!");
        } else {
          body = Text("No more Data");
        }
        return Container(
          height: 55.0,
          child: Center(child: body),
        );
      },
    );
  }
}

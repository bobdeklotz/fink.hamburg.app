/*
import 'package:finkapp/main.dart';
import 'package:finkapp/models/article.dart';
import 'package:finkapp/models/favorite_state.dart';
import 'package:finkapp/publish_subject_injector.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class ArticlePageWidget extends StatefulWidget {
  final Article _article;

  ArticlePageWidget(this._article);

  @override
  State<StatefulWidget> createState() => ArticlePageState(_article);
}

class ArticlePageState extends State<ArticlePageWidget> {
  final Article _article;
  bool _isFavorite = false;

  ArticlePageState(this._article);

  @override
  void initState() {
    super.initState();
    print('Launching PluginWebView with $_article…');
    PublishSubjectInjector.provideFavoriteStatusStream()
        .listen((favoriteState) {
      if (favoriteState.article.id == _article.id) {
        _isFavorite = favoriteState.isFavorite;
      }
    });
    PublishSubjectInjector.provideFavoriteStatusQueryStream()
        .sink
        .add(_article);
  }

  @override
  Widget build(BuildContext context) {
    //flutterWebviewPlugin.launch(url);
    return WebviewScaffold(
        url: _article.link,
        appBar: AppBar(
            title: Text(_article.title),
            backgroundColor: FinkApp.ACCENT_COLOR,
            actions: <Widget>[
              IconButton(
                  icon: _drawFavoriteIcon(), onPressed: () => _changeFavored())
            ]));
  }

  @override
  void dispose() {
    super.dispose();
    PublishSubjectInjector.provideFavoriteStatusStream().close();
  }

  Icon _drawFavoriteIcon() {
    if (_isFavorite) {
      return Icon(Icons.star, color: Colors.white);
    } else {
      return Icon(Icons.star_border, color: Colors.white);
    }
  }

  void _changeFavored() {
    _isFavorite = !_isFavorite;
    PublishSubjectInjector.provideFavoriteChangedStream()
        .sink
        .add(FavoriteState(_article, _isFavorite));
    setState(() {});
  }
}*/

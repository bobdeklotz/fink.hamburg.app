import 'dart:async';

import 'package:finkapp/models/article_query.dart';
import 'package:finkapp/models/article_state.dart';
import 'package:finkapp/services/api_service.dart';
import 'package:finkapp/services/bookmark_service.dart';

class ArticleService {
  final ApiService _apiService;
  final BookmarkService _bookmarkService;

  ArticleService(this._apiService, this._bookmarkService);

  Future<ArticleState> fetchArticles(ArticleQuery articleQuery) async {
    print(
        'Fetching articles with department ${articleQuery.department.toString()}, '
        'pageNo ${articleQuery.pageNumber} and search ${articleQuery.searchQueryString}');
    if (articleQuery.department.name == 'Lesezeichen') {
      ArticleState bookmarkState = await _bookmarkService.loadBookmarks();
      return bookmarkState;
    } else {
      ArticleState articleState = await _apiService.loadArticles(articleQuery);
      return articleState;
    }
  }
}

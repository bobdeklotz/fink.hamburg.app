import 'package:finkapp/services/api_service.dart';
import 'package:finkapp/services/article_service.dart';
import 'package:finkapp/services/bookmark_service.dart';

class ServiceInjector {
  static ArticleService _articleService;
  static ApiService _apiService;
  static BookmarkService _bookmarkService;

  static void init() {
    _apiService = ApiService();
    _bookmarkService = BookmarkService();
    _articleService = ArticleService(_apiService, _bookmarkService);
  }

  static ArticleService provideArticleService() {
    return _articleService;
  }

  static ApiService provideApiService() {
    return _apiService;
  }

  static BookmarkService provideBookmarkService() {
    return _bookmarkService;
  }
}

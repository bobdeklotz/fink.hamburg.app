import 'dart:convert';

import 'package:finkapp/main.dart';
import 'package:finkapp/models/article.dart';
import 'package:finkapp/models/article_list.dart';
import 'package:finkapp/models/article_query.dart';
import 'package:finkapp/models/article_state.dart';
import 'package:finkapp/models/department.dart';
import 'package:http/http.dart' as http;

class ApiService {
  final int _articlesPerPage = FinkApp.ARTICLES_PER_PAGE;
  final String _baseUrl = FinkApp.BASE_URL;
  final String _thumbnailSize = FinkApp.THUMBNAIL_SIZE;

  Future<ArticleState> loadArticles(ArticleQuery articleQuery) async {
    int pageNumber = articleQuery.pageNumber;
    Department department = articleQuery.department;
    String searchQueryString = articleQuery.searchQueryString;
    String url = '$_baseUrl/posts?per_page=$_articlesPerPage&page=$pageNumber';
    if (department.id != null) {
      url += '&categories=${department.id}';
    }
    if (searchQueryString != null) {
      url += '&search=$searchQueryString';
    }
    final response = await http.get(url);

    if (response.statusCode == 200) {
      List<Article> resultList =
          ArticleList.fromJson(json.decode(response.body)).articles;

      resultList = await Future.wait(
          resultList.map((article) => _loadThumbnailLink(article)));
      return ArticleLoadedState(pageNumber, resultList);
    } else {
      return ArticleErrorState(articleQuery.pageNumber, response.statusCode);
    }
  }

  Future<Article> _loadThumbnailLink(Article article) async {
    int mediaId = article.mediaId;
    if (mediaId == 0 || mediaId == null) {
      article.linkToThumbnail = '';
    } else {
      String url = '$_baseUrl/media/$mediaId';
      final response = await http.get(url);
      if (response.statusCode == 200) {
        try {
          Map<String, dynamic> responseJson = json.decode(response.body);
          String link = responseJson['media_details']['sizes'][_thumbnailSize]
              ['source_url'];
          article.linkToThumbnail = link;
        } on NoSuchMethodError {
          print('Found article without thumbnail: $article');
          article.linkToThumbnail = '';
        }
      } else {
        article.linkToThumbnail = '';
      }
    }
    return article;
  }
}

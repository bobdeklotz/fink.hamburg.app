import 'dart:convert';

import 'package:finkapp/models/article.dart';
import 'package:finkapp/models/article_state.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BookmarkService {
  static const _FAVORITES_KEY = 'Bookmarks';

  Future<List<Article>> _getBookmarks() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> favsFromSP = prefs.getStringList(_FAVORITES_KEY);
    if (favsFromSP == null) {
      favsFromSP = List();
    }
    List<Article> favs = favsFromSP
        .map((articleJson) => Article.fromJson(json.decode(articleJson)))
        .toList();
    return favs;
  }

  Future<bool> _storeBookmarks(List<Article> favs) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setStringList(_FAVORITES_KEY,
        favs.map((article) => json.encode(article.toJson())).toList());
  }

  Future<bool> addBookmark(Article article) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<Article> favs = await _getBookmarks();
    favs.insert(0, article);
    _storeBookmarks(favs.toList());
    return prefs.setBool(article.id.toString(), true);
  }

  Future<bool> removeBookmark(Article article) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<Article> favs = await _getBookmarks();
    favs.remove(article);
    _storeBookmarks(favs);
    return prefs.remove(article.id.toString());
  }

  Future<bool> isBookmark(Article article) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var isFav = prefs.getBool(article.id.toString());
    return isFav != null;
  }

  Future<ArticleState> loadBookmarks() async {
    List<Article> favs = await _getBookmarks();
    return ArticleLoadedState(1, favs);
  }
}

// This is a basic Flutter widget test.
// To perform an interaction with a widget in your test, use the WidgetTester utility that Flutter
// provides. For example, you can send tap and scroll gestures. You can also use WidgetTester to
// find child widgets in the widget tree, read text, and verify that the values of widget properties
// are correct.
// See also: https://proandroiddev.com/testing-flutter-applications-f961969da86a

import 'package:finkapp/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('Appbar smoke test', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    await tester.pumpWidget(new FinkApp());

    final Finder appTitle = find.text('FINK.HAMBURG');

    expect(appTitle, findsOneWidget);
    expect(find.byType(AppBar), findsOneWidget);

    for (int i = 0; i < 5; i++) {
      await tester.tap(appTitle);
      // expect no change
      expect(appTitle, findsOneWidget);
      expect(find.byType(AppBar), findsOneWidget);
    }
  });
}

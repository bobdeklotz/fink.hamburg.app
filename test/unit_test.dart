import 'dart:convert';
import 'dart:io';

import 'package:finkapp/models/article_list.dart';
import 'package:test/test.dart';

void main() {
  test('Article Decoding Test', () {
    // test_data.json contains an example request of 5 articles
    File testJson = File('${Directory.current.path}/test/test_data.json');
    String jsonString = testJson.readAsStringSync();
    List<dynamic> jsonList = json.decode(jsonString);
    expect(jsonList.length, equals(5));
    ArticleList articleList = ArticleList.fromJson(jsonList);
    expect(articleList.articles.length, equals(5));
  });
}
